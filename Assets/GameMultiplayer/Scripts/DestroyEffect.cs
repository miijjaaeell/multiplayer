using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class DestroyEffect : NetworkBehaviour
{
    private void Start()
    {
        Destroy(gameObject, 1f);
    }
    public override void OnStartServer()
    {
        base.OnStartServer();
        StartCoroutine(destruir());
        Destroy(gameObject,1f);
        NetworkServer.Destroy(gameObject);
    }
    [Server]
    IEnumerator destruir()
    {
        yield return new WaitForSeconds(1f);
        NetworkServer.Destroy(gameObject);
    }
}
