using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class PlayerMov : NetworkBehaviour
{
    NewInputs playerinput;
    public int maxScore = 5;

    [Header("Setup")]
    public Color player1Color = Color.green;
    public Color player2Color = Color.red;

    [Header("Stats")]
    [SyncVar] public int health;
    public int id;
    public int maxHealth;
    public float moveSpeed;
    //public float turnSpeed;              
    public float reloadSpeed;
    private float reloadTimer;
    public Slider BarraVida;

    [Header("Scores")]
    public int player1Score;                           
    public int player2Score;

    [HideInInspector]
    public Vector3 direction;
    public Vector2 _Movimiento;

    [Header("Bools")]
    public bool canMove;                   
    public bool canShoot;

    [Header("Spawn Points")]
    public List<GameObject> spawnPoints = new List<GameObject>();

    [Header("Components / Objects")]
    public Rigidbody2D rig;
    public GameObject projectile;
    public Transform muzzle;
    public Camera _Camera;
    Vector2 _MousePos;
    public Disparo projScript;
    public GameObject deathParticleEffect;
    public GameObject Player1win;
    public GameObject Player2win;
    

    public override void OnStartServer()
    {
        base.OnStartServer();
        health = maxHealth;
        canMove = true;
        canShoot = true;
    }
    private void Awake()
    {
        playerinput = new NewInputs();
        rig = GetComponent<Rigidbody2D>();
    }
    void OnEnable()
    {
        playerinput.Enable();
        playerinput.Player.Movimiento.performed += OnMovimiento;
        playerinput.Player.Movimiento.canceled += OnMovimiento;

        playerinput.Player.MousePos.performed += OnMousePos;
        playerinput.Player.MousePos.canceled += OnMousePos;

        playerinput.Player.Shoot.performed += OnShoot;
        playerinput.Player.Shoot.canceled += OnShoot;
    }
    private void OnDisable()
    {
        playerinput.Disable();
    }
    void OnMovimiento(InputAction.CallbackContext context)
    {
        _Movimiento = context.ReadValue<Vector2>();
    }
    void OnMousePos(InputAction.CallbackContext context)
    {
        _MousePos = _Camera.ScreenToWorldPoint (context.ReadValue<Vector2>());
    }
    void OnShoot(InputAction.CallbackContext context)
    {
        Shoot();
    }
    private void FixedUpdate()
    {
        rig.AddForce(_Movimiento * moveSpeed);

        Vector2 faceDirection = _MousePos - rig.position;
        float angle = Mathf.Atan2(faceDirection.y, faceDirection.x) * Mathf.Rad2Deg - 90;
        rig.MoveRotation(angle);
    }
    void Update()
    {
        BarraVida.value = health;
        reloadTimer += Time.deltaTime;
        CambioColor();

        if (player2Score >= maxScore)
        {
            canShoot = false;
            WinGame(1);
        }
        if (player1Score >= maxScore)
        {
            canShoot = false;
            WinGame(0);
        }
        
    }
    [ClientRpc]
    void ActualizarColor(int id, Color color1, Color color2)
    {
        if (id==0)
        {
            gameObject.GetComponent<SpriteRenderer>().color = color1;
        }
        if (id == 1)
        {
            gameObject.GetComponent<SpriteRenderer>().color = color2;
        }
    }
    [Server]
    void CambioColor()
    {
        ActualizarColor(id, player1Color, player2Color);
    }
    //public void Move(int y)
    //{
    //    rig.velocity = direction * y * moveSpeed * Time.deltaTime;
    //}
    //public void Turn(int x)
    //{
    //    transform.Rotate(-Vector3.forward * x * turnSpeed * Time.deltaTime);
    //    direction = transform.rotation * Vector3.up;
    //}
    [Command]
    public void Shoot()
    {
        if (reloadTimer >= reloadSpeed && canShoot)
        {                                                   
            GameObject proj = Instantiate(projectile, muzzle.transform.position,transform.rotation);
            NetworkServer.Spawn(proj);
            reloadTimer = 0.0f;
        }
    }
    public void Damage(int dmg)
    {
        if (health - dmg <= 0)
        {
            Debug.Log(health);
            if (id == 0)
            {
                AddScoreP2();
                Vida();
            }
            if (id == 1)
            {
                AddScoreP1();
                Vida();
            }
            Spawn();
            Die();
        }
        else
        {
            health = health - dmg;
            Debug.Log(health);
        }
    }

    [TargetRpc]
    public void Die()
    {
        transform.position = new Vector3(0, 100, 0);
        Invoke("RespawnP", 1f);
    }

    [ClientRpc]
    public void Spawn()
    {
        GameObject deathEffect = Instantiate(deathParticleEffect, transform.position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(deathEffect);
    }
    public void RespawnP()
    {
        transform.position = spawnPoints[Random.Range(0, spawnPoints.Count)].transform.position;
    }

    [ClientRpc]
    void actualizarVida(int vida)
    {
        BarraVida.value = vida;
    }
    [Server]
    public void Vida()
    {
        health = maxHealth;
        actualizarVida(health);
    }

    [ClientRpc]
    void WinGame(int playerId)
    {
        if (playerId==0)
        {
            GameObject Winner = Instantiate(Player1win, transform.position, transform.rotation);
            NetworkServer.Spawn(Winner);
        }
        if (playerId == 1)
        {
            GameObject Winner = Instantiate(Player2win, transform.position, transform.rotation);
            NetworkServer.Spawn(Winner);
        }
    }
    [ClientRpc]
    void ScoreCanvas(int id, int s)
    {
        Debug.Log(" ID PLAYER" + id);
        GameObject t = GameObject.FindGameObjectWithTag("SP" + id);
        t.GetComponent<Text>().text = s.ToString();
    }
    [Server]
    public void AddScoreP2()
    {
        player2Score++;    
        ScoreCanvas(id, player2Score);
    }
    [Server]
    public void AddScoreP1()
    {
        player1Score++;
        ScoreCanvas(id, player1Score);
    }
}
