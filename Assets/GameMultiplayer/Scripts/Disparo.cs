using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Disparo : NetworkBehaviour
{
	[Header("Stats")]
	public int damage;
	public Rigidbody2D rig;
	public float projectileSpeed;
	public GameObject hiteffect;
	public override void OnStartServer()
	{
		base.OnStartServer();
		StartCoroutine(destruir());
	}
	void Update()
    {
		transform.Translate(Vector3.up * projectileSpeed * Time.deltaTime);
    }
	[ServerCallback]
	void OnTriggerEnter2D(Collider2D  col)
	{
		if (col.gameObject.tag == "Player")
		{
			GameObject effect = Instantiate(hiteffect, transform.position, transform.rotation);
			col.SendMessage("Damage", damage);
			NetworkServer.Spawn(effect);
			NetworkServer.Destroy(gameObject);
		}
		if (col.gameObject.tag == "Pared")
		{
			NetworkServer.Destroy(gameObject);
		}
	}
	[Server]
	IEnumerator destruir()
	{
		yield return new WaitForSeconds(3f);
		NetworkServer.Destroy(gameObject);
	}

}
