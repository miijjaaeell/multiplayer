using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NetworkManagerTanks : NetworkManager
{
    public Transform leftRacketSpawn;
    public Transform rightRacketSpawn;

    

    GameObject ball;
    
    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        Transform start = numPlayers == 0 ? leftRacketSpawn : rightRacketSpawn;

        GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        player.GetComponent<PlayerMov>().id = numPlayers;
        NetworkServer.AddPlayerForConnection(conn, player);
    }
    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {
        // destroy ball
        if (ball != null)
            NetworkServer.Destroy(ball);

        // call base functionality (actually destroys the player)
        base.OnServerDisconnect(conn);
    }
}
