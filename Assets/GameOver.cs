using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class GameOver : NetworkBehaviour
{
    void Update()
    {
        Invoke("RestarGame", 3f);
    }
    public void RestarGame()
    {
        NetworkManager.singleton.StopClient();
        NetworkManager.singleton.StopHost();
        SceneManager.LoadScene(0);
    }
}
