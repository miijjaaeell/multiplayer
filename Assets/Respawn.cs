using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Respawn : NetworkBehaviour
{
    [Header("Spawn Points")]
    public List<GameObject> spawnPoints = new List<GameObject>();
    public PlayerMov player;

    [ClientRpc]
    public void RespawnP()
    {
        player.canMove = true;
        player.canShoot = true;

        player.health = player.maxHealth;

        transform.position = spawnPoints[Random.Range(0, spawnPoints.Count)].transform.position; 
    }

    [Server]
    public IEnumerator RespawnTimer()
    {
        yield return new WaitForSeconds(1f); 
        RespawnP();                                          
    }
}
